<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    //
    public function index()
    {
        $nama = "Rizky Saraan";
        $materi = ["web design","ios","android","web backend"];
        return view('biodata', ["nama" => $nama ,"materi"=>$materi]);
    }
}
