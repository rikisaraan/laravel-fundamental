<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hallo', function () {
    return "Hallo selamat datang di laravel app";
});

Route::get('profil','CobaController@profil'); //routing dengan controller
Route::get('biodata','BiodataController@index'); //routing dengan controller

Route::get('/blog','BlogController@home'); //routing dengan controller
Route::get('/blog/tentang','BlogController@tentang'); //routing dengan controller
Route::get('/blog/kontak','BlogController@kontak'); //routing dengan controller

