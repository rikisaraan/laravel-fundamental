<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    
    <link rel="stylesheet" href="{{asset('assets/dist/css/bootstrap.min.css')}}">
</head>
<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top"|id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="{{ url('/blog')}}">Belajar Koding</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/blog')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{url('/blog/tentang') }}">Tentang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{url('/blog/kontak') }}">Kontak</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END : Navigation -->

    <!-- Footer -->
    <footer class="py-5 bg-info"> 
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; BelajarKoding 2019</p>
        </div>
    </footer>
    <!-- END : Footer -->
    
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('assets/dist/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/dist/js/bootstrap.bundle.min.js') }}"></script>

</body>
</html>